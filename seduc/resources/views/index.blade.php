@include('header')




<div class="parallax-container">

	<div class="parallax"><img src="{{URL::asset('img/SEDUC.png')}}" style="width: 135%;"></div>

</div>

<div class="section white">
	<div class="row container">
		<h2 class="header Titulo">Secretaria</h2>
		<p class="grey-text text-darken-3 lighten-3">Seguir uma política pública de educação cujo vetor primordial seja a qualidade de ensino, Adotar um plano de ações estratégicas que envolve a humanização do ensino, a valorização dos trabalhadores da educação e a integração da comunidade; Cumprir um modelo de gestão democrática e participativa para uma educação emancipadora. Visão Estratégica...</p>
		<a href="apresentacao">Ver mais</a>
	</div>
</div>


<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/imagemTeste.png')}}"></div>
</div>

<!--div class="section white">
	<div class="row container">
		<h2 class="header Titulo">Diretoria</h2>
		<p class="grey-text text-darken-3 lighten-3"> 
			PRESIDENTE <br>
			VICE-PRESIDENTE  <br>
			SECRETÁRIA <br>
			PRESIDENTE DA CÂMARA DE EDUCAÇÃO INFANTIL <br> 
		PRESIDENTE DA CÂMARA DE ENSINO FUNDAMENTAL </p>
		<a href="diretoria">Ver mais</a>
	</div>

</div>

<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/pracamaraca.jpg')}}"></div>
</div-->

<div class="section white">
	<div class="row container">
		<h3 class="header titulo">Sistemas</h3>
		<p class="grey-text text-darken-3 lighten-3 texto"> 
			SGE
			SPC
			CLM
		</p>
		<a href="sistemas" class="texto">Ver mais</a>
	</div>
</div>


<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/imagem9.jpg')}}"></div>
</div>

<!--div class="section white">
	<div class="row container">
		<h2 class="header Titulo">Resoluções/pareceres</h2>
		<p class="grey-text text-darken-3 lighten-3"> Na página Resoluções e Pareceres estão disponíveis a legislação do CME de Maracanaú que regulamenta a educação infantil, ensino fundamental e etc, e os principais pareceres aprovados pelo CME. </p>
		<a href="resolucoes">Ver mais</a>
	</div>
</div>




<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/igrejadecima.png')}}"></div>
</div-->

<div class="section white">
	<div class="row container">
		<h2 class="header titulo" style="width:100%;">Noticias</h2>
		<p class="grey-text text-darken-3 lighten-3 texto"> Noticias de Maracanaú.</p>
		<a href="postagens" class="texto">Ver mais</a>
	</div>
</div>


<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/imagem15.jpg')}}"></div>
</div>


@include('footer')