@include('hd.headerAdmin')

<style type="text/css">
	
	textarea{
		resize: none;
		height: 50vh;
		background-color: #ecf0f1;
		border-style:1px solid black;
		border-bottom-color: black;
		border-right-color: black;
		border-radius: 5px;
		margin-bottom: 1%;
		//background-image: url("{{URL::asset('img/Fotoram.png')}}");
		background-size: 200px;
		//background-repeat: no-repeat;
		background-position: center;
		
	}

	textarea:background-image{
		opacity: 0.5;
	}

</style>



<div class="row" style="margin: 8% 0% 0% 26%;">

	<div class="col s10">



		<section id="center">
			<h2 class="header Titulo">Postagem</h2>	

			<form id="form" method="POST" action="{{ route('noticia.inserir')}}" enctype="multipart/form-data"/>
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			@if(session('sucesso'))
			<div class="alert alert success">
				{{session('sucesso')}}
			</div>
			@elseif(session('falha'))
			<div class="alert alert danger">
				{{session('falha')}}
			</div>
			@endif 

			<table > 
				<tbody>
					<label>Título</label>
					<input type="text" name="titulo" required="">
					<label>Imagem</label>

					<div class="file-field input-field">
						<div class="btn" style="background-color: #2979ff;">
							<span>File</span>
							<input  type="file"  name="post" accept="image/x-png,image/gif,image/jpeg">
						</div>

						<div class="file-path-wrapper">
							<input class="file-path validate" type="text">
						</div>
					</div>
						<label>Anexo</label>
						<div class="file-field input-field">
						<div class="btn" style="background-color: #2979ff;">
							<span>File</span>
							<input  type="file"  name="anexo" accept="image/x-png,image/gif,image/jpeg">
						</div>

						<div class="file-path-wrapper">
							<input class="file-path validate" type="text">
						</div>
					</div>

					<label>Escreva o conteúdo aqui</label>
					<div>
                            <textarea name="conteudo" required="" id="editor"></textarea>
					</div>
					<label>Data de Postagem</label>
					<div>
						<input type="date" name="data" required="">
					</div>
					<div style="margin:0% 0% 2% 0%;">
						<button class="btn waves-effect waves-light" type="submit" style="background-color: #2979ff;;">Enviar
							<i class="material-icons right">send</i>
						</button>
					</div>
				</tbody>
			</table>
		</form>
	</section>
</div>

<div class="col s2">

</div>
</div>


<script type="text/javascript" src="{{URL::asset('/ckeditor/ckeditor.js')}}"></script>
<script type="text/javascript">
    CKEDITOR.replace( 'editor' );
</script>
@include('footer.footerAdmin')