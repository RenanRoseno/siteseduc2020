@include('hd.headerAdmin')

<div class="container">
	<div class="row">
		<div class="row">

			<div class="col s2">

			</div>
			<div class="col s10">
				<h2 class="header Titulo" style="margin-top: 19%;">Postagens</h2>


				<section id="center" style=" margin: auto;	float: center;
				width: 120%;margin-left: 0%;">

				@if(session('sucesso'))
				<div class="alert alert success">
					{{session('sucesso')}}
				</div>
				@elseif(session('falha'))
				<div class="alert alert danger">
					{{session('falha')}}
				</div>
				@endif
				<table>
					<a href="{{ route('noticia.view')}}" class="waves-effect waves-light btn" style="margin:2% 0% 0% 25%;background: #2979ff; width: 50%;">Cadastrar Noticia</a>
				</table>
				<table class="responsive-table">

					<thead>
						<tr>
							<th>Titulo</th>
							<th>Data</th>
							<th>Editar</th>
							<th>Excluir</th>
						</tr>
					</thead>

					<tbody>
						@foreach($post as $postagem)
						<tr>
							<td>{{$postagem->titulo}}</td>
							<td><?php echo date("d/m/Y",strtotime($postagem->data));?></td>
							<td><a href="{{ route('noticia.editar', $postagem->id)}}"><i class="material-icons" style="color: #2979ff;">edit</i></a></td>
							<td>
								<a class="modal-trigger" href="#modal{{$postagem->id}}"><i class="smal material-icons" style="color: #2979ff;">delete</i></a>

								<div id="modal{{$postagem->id}}" class="modal" style="margin: 0% 0% 0% 30%;">
									<div class="modal-content">
										<h4>Deletar</h4>
										<p>tem certeza que você quer apagar esse usuário?</p>
									</div>
									<div class="modal-footer">
										<a  href="{{ route('noticia.excluir', $postagem->id)}}" class="modal-close waves-effect waves-light btn" style="background-color: green;">SIM</a>
										<a  href="#" class="modal-close waves-effect waves-light btn" style="background-color: red;">NÃO</a>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>


				</table>

				



			</section>


		</div>

	</div>
</div>
</div>




@include('footer.footerAdmin')





