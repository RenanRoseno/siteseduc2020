@include('header')

<style type="text/css">
	

	p{

		text-align: justify;
		line-height: 1.4;
		

	}

	.ok{

		text-decoration: underline;
	}




</style>

<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/Principal.png')}}" class="topo"></div>
</div>

<div class="section white" style="	padding: 0px 50px;">
	<div class="row container">
		<h2 class="header Titulo">Pareceres</h2>

		@foreach($pareceres as $parecer)

		<div class="row container left">
			<div class="col s8">

				<div class="row">
					<div class="col s6"><b>Título: </b>{{$parecer->titulo}}</div>
				</div>
				<div class="divider"></div>
			</div>
			<div class="col s2">
				<a href="/crudPareceres/downloadUsuario/{{$parecer->id}}" class="btn-floating"><i class="material-icons" style="background-color:#2979ff; ">file_download</i></a>
			</div>
		</div>
		@endforeach

		


	</div>
</div>

@include('footer')
