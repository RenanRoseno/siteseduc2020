	@include('header')


<style type="text/css">
	

	p, h4{

		text-align: justify;
		line-height: 1.1;
		font-size: 30px;

		

	}

	.img{
		height: 240px;
		width: 360px;
		
	}
	
	.titulo{
		color: black;
	}

</style>

<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/SEDUC.png')}}" class="topo"></div>
</div>

<div class="section white" style="	padding: 0px 50px;">

	<h2 class="titulo">Notícias</h2>
	@foreach($posts as $postagem)
	<div class="row container left">
		<hr class="style1" style="border-top: 1px solid #8c8b8b;
		border-bottom: 1px solid #fff;margin-top: -10px; opacity: 0.8;">
		<div class="col s6">
			
			<a href="{{route('postagem.id',$postagem->id)}}"><img class="img" onError="this.onerror=null;this.src='{{URL::asset('post/0.png')}}';" src="{{URL::asset('post/'.$postagem->id.'.png')}}"></a>
			
		</div>

		<div class="col s6">
			<a href="posts/{{$postagem->id}}" ><h4 class="titulo">{{$postagem->titulo}}</h4></a>
			<label><i  class="material-icons tiny">date_range</i><?php echo date("d/m/Y",strtotime($postagem->data));?>
		</label>
	</div>



</div>

@endforeach
</div>


@include('footer')