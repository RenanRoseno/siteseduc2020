@include('header')


<style type="text/css">
	

	p{

		text-align: justify;
		line-height: 1.4;
		

	}

	.ok{

		text-decoration: underline;
	}




</style>

<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/SEDUC.png')}}" class="topo"></div>
</div>

<div class="section white" style="	padding: 0px 50px;">
	<div class="row container">
		<h4 class="header Titulo">Competências</h4>
		<p class="grey-text text-darken-3 lighten-3">
			Seguir uma política pública de educação cujo vetor primordial seja a qualidade de ensino, Adotar um plano de ações estratégicas que envolve a humanização do ensino, a valorização dos trabalhadores da educação e a integração da comunidade; Cumprir um modelo de gestão democrática e participativa para uma educação emancipadora. Visão Estratégica • Qualidade: Excelência na oferta dos serviços educacionais e culturais e no cumprimento da função social da escola. • Equidade:Garantia do acesso, permanência e sucesso no processo educacional. • Compromisso:Projeto educativo comprometido com a transformação social. • Democratização: Gestão participativa, autônoma com sustentabilidade dos serviços e dos processos. • Ética: Valorização da pessoa humana e probidade na gestão da educação. • Solidariedade: Fortalecimento do senso de coletividade, companheirismo, compromisso e cooperação. • Identidade: Valorização da cultura local, destacando o desenvolvimento industrial, contextualidade à cultura universal e ao ambiente natural. Visão de Futuro: Seremos uma Secretaria de excelência na prestação dos serviços educacionais, de referência nacional e internacional, assegurando à escola e aos demais usuários um elevado padrão de qualidade nas ações desenvolvidas, caracterizadas por atitudes solidárias, democráticas, éticas e empreendedoras. Missão: Garantir o atendimento aos serviços educacionais, culturais e desportivos com qualidade e equidade, proporcionando a formação por meio de um projeto educativo inovador e criativo, num ambiente democrático e de valorização humana.cro e pequenas empresas e negócios.
		</p><br>
		<h4 class="header Titulo">Ações</h4>
		<p class="grey-text text-darken-3 lighten-3">
			<b>Conferência Infanto-juvenil do Meio Ambiente</b> Os alunos do Ensino Fundamental do 6° ao 9ª ano das escolas públicas municipais participam anualmente da Conferência Infanto-juvenil do Meio Ambiente. As atividades são realizadas nas escolas, onde os alunos participam de atividades durante de discussão sobre temas relacionados ao assuntos: mudanças climáticas, biodiversidade, diversidade étnico-racial e segurança alimentar e nutricional. Além dos alunos das escolas municipais, participam também os povos indígenas, comunidades quilombolas, povos de assentamentos, além de crianças de rua. Deparados os temas, os alunos refletem os principais problemas locais ligados a cada um deles e tentam encontrar alternativas para solucioná-los , gerando relatos e projetos. Os projetos são enviados para a Secretaria de Educação do Estado que selecionará, em todo o Ceará, 25 representantes para participar da Conferência Nacional, realizada a cada 3 anos, em Brasília.

			Semana da Pátria Maracanaú promove anualmente uma das mais belas homenagens à Independência do Brasil. A Semana da Pátria reúne a comunidade, técnicos da Secretaria de Educação, escolas públicas e particulares de Maracanaú na preparação antecipada para organização de todo a homenagem. O Desfile Cívico, também conta com a presença das Forças Armadas e dos Bombeiros. O evento ocorre nas ruas do centro da cidade e nos bairros durante todo o dia 7 de setembro.<br><br>

			<b>Semana da Pátria</b> Maracanaú promove anualmente uma das mais belas homenagens à Independência do Brasil. A Semana da Pátria reúne a comunidade, técnicos da Secretaria de Educação, escolas públicas e particulares de Maracanaú na preparação antecipada para organização de todo a homenagem. O Desfile Cívico, também conta com a presença das Forças Armadas e dos Bombeiros. O evento ocorre nas ruas do centro da cidade e nos bairros durante todo o dia 7 de setembro.<br><br>

			<b>PNAIC</b> O Pacto Nacional pela Alfabetização na Idade Certa é um compromisso formal assumido pelos governos federal, do Distrito Federal, dos estados e municípios de assegurar que todas as crianças estejam alfabetizadas até os oito anos de idade, ao final do 3º ano do ensino fundamental. Pacto esse, que Maracanaú adere e No Pacto Nacional pela Alfabetização na Idade Certa, quatro princípios centrais serão considerados ao longo do desenvolvimento do trabalho pedagógico: 1. o Sistema de Escrita Alfabética é complexo e exige um ensino sistemático e problematizador; 2. o desenvolvimento das capacidades de leitura e de produção de textos ocorre durante todo o processo de escolarização, mas deve ser iniciado logo no início da Educação Básica, garantindo acesso precoce a gêneros discursivos de circulação social e a situações de interação em que as crianças se reconheçam como protagonistas de suas próprias histórias; 3. conhecimentos oriundos das diferentes áreas podem e devem ser apropriados pelas crianças, de modo que elas possam ouvir, falar, ler, escrever sobre temas diversos e agir na sociedade; 4. a ludicidade e o cuidado com as crianças são condições básicas nos processos de ensino e de aprendizagem.<br><br>

			<b>Formação permanente de gestores</b> A Formação permanente de gestores é uma atividade constante da Secretaria de Educação. Iniciado com a seleção de gestores e novas formações, o processo tem buscado potencializar os talentos, fortalecer os relacionamentos e intensificar as capacidades gestoras de todos. Ocorridas mensalmente, as formações e reuniões com os gestores tendem a aproximar mais ainda a Secretaria de Educação e as escolas, favorecendo todo o processo educativo. Exercitando a prática da democracia e estimulando a participação de todos os segmentos da população, a Secretaria de Educação promove encontros sobre alternativas a uma educação de qualidade em Maracanaú, além de seminários com educadores e técnicos objetivando uma atualização permanente de todos os envolvidos no processo educativo. Em 2012 foi lançado o Plano Plurianual contendo todos os resultados e metas a serem alcançados referentes à Educação. Tendo vigência até 2021.<br><br>


			<b>Cultura de Paz</b> Construir coletivamente uma cultura de paz nas escolas de Maracanaú é o objetivo maior do projeto permanente desenvolvido nas escolas da rede pública municipal. Tem como objetivo maior promover um ambiente de amizade e respeito ao outro no ambiente escolar ampliando para a comunidade em que cada aluno convive.<br><br>


			<b>Educação especial</b> A Educação inclusiva no município de Maracanaú tem como objetivo principal garantir e implementar as políticas de inclusão, promovendo a construção de uma escola aberta para todos, que respeita e valoriza a diversidade, desenvolve práticas colaborativas, forma redes de apoio à inclusão e fomenta a participação da comunidade.<br><br>


			<b>Municipalização do Ensino Infantil</b> Prefeitura de Maracanaú assumiu a responsabilidade de fato e de direito do Ensino Infantil, oferecendo melhorias: professores de nível superior, boas instalações físicas, material didático e merenda escolar.
		</p><br>
	</div>
</div>


@include('footer')
