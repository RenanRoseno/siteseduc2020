@include('header')


<style type="text/css">
	

	.p{

		text-align: justify;
		line-height: 1.4;
		
	}

	.ok{

		text-decoration: underline;
	}

	



</style>

<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/SEDUC.png')}}" class="topo"></div>
</div>

<div class="section white" style="	padding: 0px 50px;">
	<div class="row container">
		<h2 class="header Titulo">Conselheiros</h2>
	
		<!-- informações aqui -->
			<div class="col s10">

				@foreach($representantes as $representante)

				<h6 style="text-transform: uppercase;"><b>{{$representante->desc}}</b></h6>

				<form>
					<table class="highlight">
						<thead>
							<tr>
								<th class="p" >Titular</th>
								<th class="p" style="width: 50%;">Suplente</th>
							</tr>
						</thead>
						<tbody>
							@forelse($representante->conselheiros as $conselheiro)
							<tr>
								<td class="p">{{$conselheiro->titular}}</td>
								<td class="p">{{$conselheiro->suplente}}</td>
							</tr>
							@empty

							@endforelse
						</tbody>

					</table>
				</form>
				@endforeach
			</div>


	</div>
</div>


@include('footer')
