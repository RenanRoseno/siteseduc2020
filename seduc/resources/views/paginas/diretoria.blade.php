@include('header')


<style type="text/css">
	

	p{

		text-align: justify;
		line-height: 1.4;
		

	}

	.ok{

		text-decoration: underline;
	}




</style>

<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/SEDUC.png')}}" class="topo"></div>
</div>

<div class="section white" style="	padding: 0px 50px;">
	<div class="row container">

	
		<!-- informações aqui -->

		</div>
			<div class="col s10" style="margin-bottom: 6%;">
				<h2 class="header Titulo">Diretoria</h2>

				<section id="center" style=" margin: auto;	float: center;
				width: 100%;margin-left: 0%;">

				<table class="responsive-table">
					<thead>
						<tr>
							<th>Cargo</th>
							<th>Nome</th>
							<th>Data de início</th>
							<th>Data de término</th>
						</tr>
					</thead>
					<tbody>
						@foreach($diretorias as $dire)
						<tr>
							<td><b>{{$dire->dados->descricao or 'Cargo não encontrado'}}<b></td>
							<td>{{$dire->nome}}</td>
							<td><?php echo date("d/m/Y",strtotime($dire->data_inicio));?></td>
							<td><?php echo date("d/m/Y",strtotime($dire->data_fim));?></td>
							

						</tr>
						@endforeach
					</tbody>

				</table>

		</div>


	</div>
</div>


@include('footer')
