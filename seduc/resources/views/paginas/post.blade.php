@include('header')


<style type="text/css">
	

	.p{
		font-family: url(https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap);
		text-align: justify;
		line-height: 1.4;
		text-indent: 5em;
		padding: 5px; 
		

	}

	.img{
		max-height: 600px;
		max-width: 800px;
		min-height: 400px;
		min-width: 600px;
		margin-left: 50px;
		border: 1px solid #ddd; 
		border-radius: 4px;  
		padding: 5px; 
		width: 150px; 
	}


	.img:hover {
		box-shadow: 0 0 2px 1px rgba(0, 140, 186, 0.5);
	}

	.noticia{
		text-align: justify;
		margin-left: 27%;

		max-width: 820px;
	}
	


</style>

<div class="parallax-container">
	<div class="parallax"><img src="{{URL::asset('img/SEDUC.png')}}" class="topo"></div>
</div>
<div class="col noticia">
	<h3 class="titulo" style="color: black;text-align: center"><p>{{$post->titulo}}</p></h3>
	<center><img class="img" src="{{URL::asset('post/'. $post->id.'.png')}}" onError="this.onerror=null;this.src='{{URL::asset('post/0.png')}}';"></center>

	<p class="p texto">
                        <?php 
                            echo $post->conteudo;
                        ?>
                    </p>
                    <center>
         <h5 class="titulo" style="color: black;"><p>Anexos</p></h5>        
         <div class="row container center">
						<div class="col s8">

							<div class="row">
								
								<div class="col s6"><b>Título: </b>Anexo_{{$post->id}}.pdf</div>
				
							</div>
							<div class="divider"></div>
						</div>
						<div class="col s2">

							<a href="{{route('anexo.download', $post->id)}}" class="btn-floating"><i class="material-icons" style="background-color:#2979ff; ">file_download</i></a>

						</div>
						

					</div></center>
</div>

<br><br>


<!--
<div class="section white" style="padding: 0px 50px;">

	<div class="row container">
		<div class="col s10">
		<h2 style="color: black;text-align: center"><p>{{$post->titulo}}</p></h2>
		<img class="img" src="{{URL::asset('post/'. $post->id.'.png')}}" onError="this.onerror=null;this.src='{{URL::asset('post/0.png')}}';">
		<h6><p class="p">
                        <?php 
                            echo $post->conteudo;
                        ?>
                    </p>
                </h6>
         </div>
	</div>
</div>-->
@include('footer')