@include('hd.headerAdmin')


@if(session('sucesso'))
<div class="alert alert success">
	{{session('sucesso')}}
</div>
@elseif(session('falha'))
<div class="alert alert danger">
	{{session('falha')}}
</div>
@endif 





<div class="container">
	<div class="row">
		<div class="row">

			<div class="col s2">

			</div>
			<div class="col s10">
				<h2 class="header Titulo" style="margin-top: 19%;">Diretoria</h2>

				<section id="center" style=" margin: auto;	float: center;
				width: 120%;margin-left: 0%;">

				<table class="responsive-table">
					<thead>
						<tr>
							<th>Nome</th>
							<th>Data de inicio</th>
							<th>Data de termino</th>
							<th>Cargo</th>
							<th>Editar</th>
							<th>Excluir</th>
						</tr>
					</thead>
					<tbody>
						@foreach($diretorias as $dire)
						<tr>
							<td>{{$dire->nome}}</td>
							<td><?php echo date("d/m/Y",strtotime($dire->data_inicio));?></td>
							<td><?php echo date("d/m/Y",strtotime($dire->data_fim));?></td>
							<td>{{$dire->dados->descricao or 'Cargo não encontrado'}}</td>
							<td><a href="/editarD/{{$dire->id}}"><i class="smal material-icons" style="color: #2979ff;">edit</i></a>
							</td>
							
							<td>
								<a class="modal-trigger" href="#modal{{$dire->id}}"><i class="smal material-icons" style="color: #2979ff;">delete</i></a>

								<div id="modal{{$dire->id}}" class="modal" style="margin: 0% 0% 0% 30%;">
									<div class="modal-content">
										<h4>Deletar</h4>
										<p>tem certeza que você quer apagar esse usuário?</p>
									</div>
									<div class="modal-footer">
										<a  href="/crudDiretoria/deletarD{{$dire->id}}" class="modal-close waves-effect waves-light btn" style="background-color: green;">SIM</a>
										<a  href="#" class="modal-close waves-effect waves-light btn" style="background-color: red;">NÃO</a>
									</div>
								</div>
							</td>

						</tr>
						@endforeach
					</tbody>

				</table>


				<a href="/crudDiretoria/criarD" class="waves-effect waves-light btn" style="margin:2% 0% 0% 25%;background: #2979ff; width: 50%;">Cadastrar</a>
			</section>

		</div>

	</div>
</div>
</div>




@include('footer.footerAdmin')





