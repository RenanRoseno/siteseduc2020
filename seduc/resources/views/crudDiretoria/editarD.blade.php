@include('hd.headerAdmin')


<div class="row" style="margin-top: 60px;">
	<div class="col s2">
		
	</div>
	<div class="col s10">

		<h2 class="header Titulo" style="margin-top: 19%;">Editar</h2>		

		<section id="center" style=" margin: auto;	float: center;
		width: 85%;margin-left: 12%;">


			@if(session('sucesso'))
			<div class="alert alert success">
				{{session('sucesso')}}
			</div>
			@elseif(session('falha'))
			<div class="alert alert danger">
				{{session('falha')}}
			</div>
			@endif 

			<form id="form" method="POST" action="/editarD/editD">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$editD->id}}">

			<table> 
				<tbody>
					<label>Usuário</label>
					<input type="text" name="nome" value="{{$editD->nome}}">
					<label>Data de inicio</label> 
					<input type="date" name="data_inicio" value="{{$editD->data_inicio}}">
					<label>Data de termino</label>
					<input type="date" name="data_fim" value="{{$editD->data_fim}}">
					<label>Cargo</label>

					<select id='select' name="id_cargo">
						<option value="0">Selecione</option>
						@foreach($cargos as $c)
							@if($c->id == $editD->id_cargo)
								<option value="{{$c->id}}" selected>{{$c->descricao}}</option>
							@else
								<option value="{{$c->id}}">{{$c->descricao}}</option>
							@endif
						@endforeach
					</select>
					
					<button class="btn waves-effect waves-light" type="submit" style="background-color:   #2979ff;">Enviar
						<i class="material-icons right">send</i>
					</button>
				</tbody>
			</table>
		</div>
	</form>
</section>

</div>




@include('footer.footerAdmin')

