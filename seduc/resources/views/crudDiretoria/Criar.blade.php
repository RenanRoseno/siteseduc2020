@include('hd.headerAdmin')


<div class="row" style="margin-top: 60px;">
	<div class="col s2">
		
	</div>
	<div class="col s10">

		<h2 class="header Titulo" style="margin-top: 19%;">Cadastrar</h2>		

		<section id="center" style=" margin: auto;	float: center;
		width: 85%;margin-left: 12%;">


		<form id="form" method="POST" action="/crudDiretoria/criarD">
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			@if(session('sucesso'))
			<div class="alert alert success">
				{{session('sucesso')}}
			</div>
			@elseif(session('falha'))
			<div class="alert alert danger">
				{{session('falha')}}
			</div>
			@endif 



			<table> 
				<tbody>
					<label>Usuário</label>
					<input type="text" name="nome" required="">
					<label>Data de inicio</label> 
					<input type="date" name="data_inicio" required="">
					<label>Data de termino</label>
					<input type="date" name="data_fim" required="">
					<label>Cargo</label>
					<select id='select' name="id_cargo">
						<option value="0">Selecione</option>
						@foreach($cargos as $c)
						<option value="{{$c->id}}">{{$c->descricao}}</option>
						@endforeach
					</select>
					<button class="btn waves-effect waves-light" type="submit" style="background-color: #2979ff;">Enviar
						<i class="material-icons right">send</i>
					</button>
				</tbody>
			</table>
		</div>
	</form>
</section>

</div>




@include('footer.footerAdmin')

