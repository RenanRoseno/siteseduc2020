@include('hd.headerAdmin')

<div class="row" style="margin: 8% 0% 0% 26%;">

	<div class="col s10">



		<section id="center">
			<h2 class="header Titulo">Cadastrar</h2>	

			<form id="form" method="POST" action="/crudPareceres/cadastrarParecer" enctype="multipart/form-data"/>
			<input type="hidden" name="_token" value="{{csrf_token()}}">

			@if(session('sucesso'))
			<div class="alert alert success">
				{{session('sucesso')}}
			</div>
			@elseif(session('falha'))
			<div class="alert alert danger">
				{{session('falha')}}
			</div>
			@endif 

			<table > 
				<tbody>
					<label>Título</label>
					<input type="text" name="titulo" required="">
					<label>Arquivo</label>
					<div class="file-field input-field">
						<div class="btn" style="background-color: #2979ff;">
							<span>File</span>
							<input  type="file"  name="pareceres" accept="image/x-png,image/gif,image/jpeg">
						</div>
						<div class="file-path-wrapper">
							<input class="file-path validate" type="text">
						</div>
					</div>

					<button class="btn waves-effect waves-light" type="submit" style="background-color: #2979ff;">Enviar
						<i class="material-icons right">send</i>
					</button>
				</tbody>
			</table>
		</form>
	</section>
</div>

<div class="col s2">

</div>
</div>




@include('footer.footerAdmin')