﻿<!DOCTYPE html>
<html>
<head>

	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('materialize/css/materialize.min.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="{{URL::asset('style/style.css')}}">
	<link rel="preconnect" href="https://fonts.gstatic.com">
<link href="#" rel="stylesheet">

<style type="text/css">
	.titulo, a{
		font-family: url(https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap);
		font-weight: bold;
	}

	.texto{
		font-family: url(https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap);
	}

</style>
</head>

<body>
	<nav id="nav-header teal lighten-3">
		<div class="nav-wrapper">
			<a  class="brand-logo"><img src="{{URL::asset('img/logo.png')}}" id="logo"></a>
			<a href="#" class="sidenav-trigger" data-target="mobile-links">
				<i class="material-icons">menu</i>
			</a>


			<ul class="right hide-on-med-and-down ">
				<li><a class="breadcrumb" href="{{ route('home')}}" class="nav-item active">Início</a></li>
				<li><a href="{{URL::route('apresentacao')}}" class="ho ">Secretaria</a></li>
				<!--li><a href="{{URL::route('diretoria')}}" class="ho">Diretoria</a></li-->
				<?php //	<li><a href="{{URL::route('conselheiros')}}" class="ho">Conselheiros</a></li> ?>
				<!--li><a href="{{URL::route('resolucoes')}}" class="ho">Resoluções</a></li-->
				<!--li><a href="{{URL::route('pareceres')}}" class="ho">Pareceres</a></li-->
				<!--li><a href="{{URL::route('planoDeAcao')}}" class="ho">Plano de Ação</a></li-->
				<li><a href="{{ URL::route('postagens')}}" class="ho">Notícias</a></li>
				<li><a href="{{ URL::route('sistemas')}}" class="ho">Sistemas</a></li>
				<!--li><a href="/postagens" class="ho">Videos</a></li-->
				<li><a href="{{ URL::route('admin')}}" class="ho"><i class="material-icons">account_circle</i></a></li>
			</ul>

			<!---- dropdown --->

			<ul class="sidenav" id="mobile-links">
				<li><a class="breadcrumb" href="/" class="nav-item active">Início</a></li>
				<li><a href="{{URL::route('apresentacao')}}" class="ho ">Secretaria</a></li>
				<!--li><a href="{{URL::route('diretoria')}}" class="ho">Diretoria</a></li>
				<li><a href="{{URL::route('conselheiros')}}" class="ho">Conselheiros</a></li>
				<li><a href="{{URL::route('resolucoes')}}" class="ho">Resoluções</a></li>
				<li><a href="{{URL::route('pareceres')}}" class="ho">Pareceres</a></li>
				<li><a href="{{URL::route('planoDeAcao')}}" class="ho">Plano de Ação</a></li-->
				<li><a href="{{ URL::route('postagens')}}" class="ho">Notícias</a></li>
				<li><a href="{{ URL::route('admin')}}" class="ho"><i class="material-icons">account_circle</i></a></li>

			</ul>


		</div>

	</nav>




