        @include('hd.headerLogin')



        <div class="container" style="margin:auto; margin-top: 100px; width: 55%;">
          <div class="row" >
            <div class=" col s12"><h1 style="text-align: center; color: #2979ff;">Login</h1></div>
            <form class="col s12" role="form" method="POST" action="{{ url('/login') }}">
             <div class="row" >
              {{ csrf_field() }}
              <div class="input-field col s12 form-group{{ $errors->has('password') ? ' has-error' : '' }}">



                <i class="material-icons prefix" style="color: #2979ff;">email</i>

                <input id="email" type="text" class="form-control" placeholder="Email: " name="email" value="{{ old('email') }}">

                @if ($errors->has('password'))
                <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
              </span>
              @endif

              <label for="email" class="col-md-4 control-label"></label>

          </div>

          <div class="input-field col s12 form-group{{ $errors->has('password') ? ' has-error' : '' }}">
            <i class="material-icons prefix" style="color: #2979ff;">vpn_key</i>

            <input id="password" type="password" placeholder="Senha: " class="form-control" name="password">

            @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
          @endif

          <label for="password" class="col-md-4 control-label"></label>
          <div class="form-group" >
              <div class="col-md-6 col-md-offset-4" style="margin-left: 50px; ">
                <button type="submit" class="btn btn-primary" style=" background-color: #2979ff; width: 100%;">
                 ENTRAR
             </button>


         </div>
     </div>
 </div>
</form>
</div>
</div>


