@include('hd.headerAdmin')


<div class="container">
	<div class="row">
		<div class="row">

			<div class="col s2">

			</div>
			<div class="col s10">
				<h2 class="header Titulo" style="margin-top: 19%;">Conselheiros</h2>

				<section id="center" style=" margin: auto;	float: center;
				width: 120%;margin-left: 0%;">

				<table class="responsive-table">
					<thead>
						<tr>
							<th>Cargo</th>
							<th>Titular</th>
							<th>Suplente</th>
							
							<th>Editar</th>
							<th>Excluir</th>
						</tr>
					</thead>
					<tbody>
						@foreach($conselheiros as $conselheiro)
						<tr>
							<td><b>{{$conselheiro->representantes->desc or 'Cargo não encontrado'}}</b></td>
							<td>{{$conselheiro->titular}}</td>
							<td>{{$conselheiro->suplente}}</td>
							
							<td><a href="/editarConselheiros/{{$conselheiro->id}}"><i class="smal material-icons" style="color: #2979ff;">edit</i></a>
							</td>
							
							<td>
								<a class="modal-trigger" href="#modal{{$conselheiro->id}}"><i class="smal material-icons" style="color: #2979ff;">delete</i></a>

								<div id="modal{{$conselheiro->id}}" class="modal" style="margin: 0% 0% 0% 30%;">
									<div class="modal-content">
										<h4>Deletar</h4>
										<p>tem certeza que você quer apagar esse usuário?</p>
									</div>
									<div class="modal-footer">
										<a  href="/Crudconselheiros/deletarConselheiros/{{$conselheiro->id}}" class="modal-close waves-effect waves-light btn" style="background-color: green;">SIM</a>
										<a  href="#" class="modal-close waves-effect waves-light btn" style="background-color: red;">NÃO</a>
									</div>
								</div>
							</td>

						</tr>
						@endforeach
					</tbody>

				</table>


				<a href="criarConselheiros" class="waves-effect waves-light btn" style="margin:2% 0% 0% 25%;background: #2979ff; width: 50%;">Cadastrar</a>
			</section>

		</div>

	</div>
</div>
</div>


@include('footer.footerAdmin')
