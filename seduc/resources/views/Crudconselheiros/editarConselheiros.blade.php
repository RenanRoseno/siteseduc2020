@include('hd.headerAdmin')


<div class="row" style="margin-top: 60px;">
	<div class="col s2">
		
	</div>
	<div class="col s10">

		<h2 class="header Titulo" style="margin-top: 19%;">Editar</h2>		

		<section id="center" style=" margin: auto;	float: center;
		width: 85%;margin-left: 12%;">


		@if(session('sucesso'))
		<div class="alert alert success">
			{{session('sucesso')}}
		</div>
		@elseif(session('falha'))
		<div class="alert alert danger">
			{{session('falha')}}
		</div>
		@endif 

		<form id="form" method="POST" action="/editarConselheiros">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$conselheiros->id}}">

			<table> 
				<tbody>
					<label>Titular</label>
					<input type="text" name="titular" value="{{$conselheiros->titular}}" required="">
					<label>Suplente</label>
					<input type="text" name="suplente" value="{{$conselheiros->suplente}}" required="">
					<label>Cargo</label>


					<select id='select' name="id_representante">
						<option value="0">Selecione</option>
						@foreach($representantes as $representante)
						@if($representante->id == $conselheiros->id_representante)
						<option value="{{$representante->id}}" selected>{{$representante->desc}}</option>
						@else
						<option value="{{$representante->id}}">{{$representante->desc}}</option>
						@endif
						@endforeach
					</select>
					
					<button class="btn waves-effect waves-light" type="submit" style="background-color:   #2979ff;">Enviar
						<i class="material-icons right">send</i>
					</button>
				</tbody>
			</table>
		</div>
	</form>
</section>

</div>




@include('footer.footerAdmin')

