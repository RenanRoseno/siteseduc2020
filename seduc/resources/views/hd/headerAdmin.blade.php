<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{URL::asset('materialize/css/materialize.min.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="{{URL::asset('style/style.css')}}">
	

	<style type="text/css">

		#btn{
			display: block;
			background: #2979ff;
		}

		#btn li a{
			color: #fff;
		}

		#btn li a:hover{
			background: #74b9ff;
			transition: 0.8s;
			cursor: pointer;
			
		}
		
		/* importantes */
		.item ul{
			width: 100%;
			list-style: none;
			overflow: hidden;
			max-height: 0;
			transition: all .4s linear;
		}

		.item input:checked ~ ul{
			height: auto;
			max-height: 500px;
			transform: all;
		}
		.item input{
			display: none;
		}


		header, main, footer {
			padding-left: 300px;
		}

		@media only screen and (max-width : 992px) {
			header, main, footer {
				padding-left: 0;
			}
		}

		#logoadmin{
			height: 110px;
			width: 40%;
			margin: 10px 0px -25px 80px;
			background-color: #2979ff;
		}

		.homeAd{
			text-align: justify;
			line-height: 1.4;
			margin: auto;

		}
		
		.img:hover{

			background-color: gray;
			opacity: 0.8;
			transition: 2s;

		}



	</style>
</head>
<body>

	<nav id="nav-header" class="admin">
		
		<div class="middle" class="nav-wrapper">

			<ul class="right hide-on-med-and-down">
				<li><a href="{{ route('home') }}"><i class=" material-icons  " style=" margin-top: 4px;">home</i></a></li>
			</ul>
			<div class="menu">

				<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons large">menu</i></a>

				<ul id="slide-out" class="sidenav sidenav-fixed">
					
					<div class="background">
						<a  class="a2" href="/admin">
							<img id="logoadmin" class="circle responsive-img" src="{{URL::asset('img/logol.png')}}">
						</a>
					</div>
					
					<li><div class="divider"></div></li>

					<div class="item">

						<label><li><a href="{{ route('usuarios.visualizar')}}">Usuários</a></li></label>

					</div>

					<li><div class="divider"></div></li>

					<div class="item">

						<label><li><a href="{{ route('noticias.visualizar')}}">Noticias</a></li></label>

					</div>

					<li><div class="divider"></div></li>

					<!--div class="item">

						<input type="checkbox" id="check4">
						<label for="check4"><li><a>Páginas</a></li></label>
						<ul id="btn">
							<li><a href="crudDiretoria/visualizarD">Diretoria</a></li>
							<li><a href="Crudconselheiros/indexConselheiros">Conselheiros</a></li>
							<li><a href="crudResolucoes/cadastro">Resoluções/Pareceres</a></li>
							<li><a href="crudPlanosDeAcoes/visualizarPlanoDeAcao">Plano de Ação</a></li>
						</ul>

					</div>

					<li><div class="divider"></div></li-->
					<li><a href="logout"><i class="material-icons" style="margin-left: 65px;position: absolute;">exit_to_app</i>Logout</a></li>

				</ul>


				<!-- responsivo -->

				<ul id="slide-out" class="sidenav sidenav-fixed">
					<div class="background">
						<a  class="a2" href="admin">
							<img id="logoadmin" class="circle responsive-img" src="{{URL::asset('img/logol.png')}}">
						</a>
					</div>


					<li><div class="divider"></div></li>


					<div class="item">

						
						<label><li><a href="{{ route('usuarios.visualizar')}}">Usuários</a></li></label>

					</div>

					<li><div class="divider"></div></li>

					<div class="item">

						
						<label><li><a href="{{ route('noticias.visualizar')}}">Noticias</a></li></label>

					</div>

					<li><div class="divider"></div></li>

					<!--div class="item">

						<input type="checkbox" id="check2">
						<label for="check2"><li><a>Páginas</a></li></label>
						<ul id="btn">
							<li><a href="/crudDiretoria/visualizarD">Diretoria</a></li>
							<li><a href="/Crudconselheiros/indexConselheiros">Conselheiros</a></li>
							<li><a href="/crudResolucoes/visualizarResolucoes">Resoluções</a></li>
							<li><a href="/crudPareceres/visualizarPareceres">Pareceres</a></li>
							<li><a href="/crudPlanosDeAcoes/visualizarPlanoDeAcao">Plano de Ação</a></li>
							<li><a href="/crudDiretoria/visualizarD">Diretoria</a></li>
							<li><a href="/Crudconselheiros/indexConselheiros">Conselheiros</a></li>
							<li><a href="/crudResolucoes/visualizarResolucoes">Resoluções/Pareceres</a></li>
							<li><a href="/crudPlanosDeAcoes/visualizarPlanoDeAcao">Plano de Ação</a></li>
						</ul>

					</div>


					<li><div class="divider"></div></li-->
					<li><a href="{{ route('logout')}}"><i class="material-icons" style="margin-left: 65px;position: absolute;">exit_to_app</i>Logout</a></li>


				</ul>

			</div>
		</div>
	</nav>
