<!doctype html>
<html lang="en">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">



  <link rel="stylesheet" type="text/css" href="{{URL::asset('dist/summernote-bs4.css')}}">

  <link rel="stylesheet" type="text/css" href="{{URL::asset('bootstrap/css/bootstrap.css')}}">
  <title>Hello, world!</title>
</head>
<body>



  <div class="row">
    <div class="col-lg-12">

      <form action="">

        <div class="form-group">
          <label for="">Description</label>
          <textarea name="" id="description" rows="10" class="form-group"></textarea>
        </div>
        <button type="submit" class="btn btn-submit">submit</button>

      </form>
      
    </div>


  </div>



  <script src="{{URL::asset('bootstrap/js/jquery.js')}}" ></script>
  <script src="{{URL::asset('bootstrap/js/bootstrap.min.js')}}"></script>

  <script src="{{URL::asset('dist/summernote.min.js')}}"></script>

  <script> 

    $(document).ready(function(){

        $('#description').summernote();

    });


  </script>


</body>
</html>