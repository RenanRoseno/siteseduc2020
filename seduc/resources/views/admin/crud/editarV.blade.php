@include('hd.headerAdmin')




<div class="row" style="margin-top: 60px;">
	<div class="col s2">
		
	</div>
	<div class="col s10">

		<h2 class="header Titulo" style="margin-top: 19%;">Editar</h2>		

		<section id="center" style=" margin: auto;	float: center;
		width: 85%;margin-left: 12%;">


		<form id="form" method="POST" action="{{route('usuario.atualizar')}}">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			<input type="hidden" name="id" value="{{$editar->id}}">

			@if(session('sucesso'))
			<div class="alert alert success">
				{{session('sucesso')}}
			</div>
			@elseif(session('falha'))
			<div class="alert alert danger">
				{{session('falha')}}
			</div>
			@endif 



			<table> 
				<tbody>
					<label>Usuário</label>
					<input type="text" name="name" value="{{$editar->name}}" required="">
					<label>email</label> 
					<input type="text" name="email" value="{{$editar->email}}" required="">
					<label>senha:</label>
					<input type="password" name="password" value="" required="">
					<label>tipo</label>

					<select id='select' name="id_tipo">
						<option value="0">Selecione</option>
						@foreach($tipos as $tipo)
							@if($tipo->id == $editar->id_tipo)
								<option value="{{$tipo->id}}" selected>{{$tipo->descricao}}</option>
							@else
								<option value="{{$tipo->id}}">{{$tipo->descricao}}</option>
							@endif
						@endforeach
					</select>

					<button class="btn waves-effect waves-light" type="submit" style="background-color: #2979ff;">Enviar
						<i class="material-icons right">send</i>
					</button>
				</tbody>
			</table>
		</div>
	</form>
</section>

</div>



@include('footer.footerAdmin')