@include('hd.headerAdmin')




<div class="container">
	<div class="row">
		<div class="row">

			<div class="col s2">

			</div>
			<div class="col s10">
				<h2 class="header Titulo" style="margin-top: 19%;">Usuarios</h2>

				<section id="center" style=" margin: auto;	float: center;
				width: 120%;margin-left: 0%;">

				<table class="responsive-table">
					<thead>

						@if(session('sucesso'))
						<div class="alert alert success">
							{{session('sucesso')}}
						</div>
						@elseif(session('falha'))
						<div class="alert alert danger">
							{{session('falha')}}
						</div>
						@endif 

						
						<tr>
							<th>id</th>
							<th>nome</th>
							<th>email</th>
							<th>tipo</th>
							<th></th>
							<th></th>

						</tr>
					</thead>
					<tbody>
						@foreach($usuarios as $usuarios)
						<tr>
							<td>{{$usuarios->id}}</td>
							<td>{{$usuarios->name}}</td>
							<td>{{$usuarios->email}}</td>
							<td>{{$usuarios->dados->descricao or 'Cargo não encontrado'}}</td>
							<td><a href="{{ route('usuario.editar', $usuarios->id)}}"><i class="smal material-icons" style="color: #2979ff;">edit</i></a>
							</td>
							<td>
								<a class="modal-trigger" href="#modal{{$usuarios->id}}"><i class="smal material-icons" style="color: #2979ff;">delete</i></a>

								<div id="modal{{$usuarios->id}}" class="modal" style="margin: 0% 0% 0% 30%;">
									<div class="modal-content">
										<h4>Deletar</h4>
										<p>tem certeza que você quer apagar esse usuário?</p>
									</div>
									<div class="modal-footer">
										<a  href="{{route('usuario.excluir', $usuarios->id)}}" class="modal-close waves-effect waves-light btn" style="background-color: green;">SIM</a>
										<a  href="#" class="modal-close waves-effect waves-light btn" style="background-color: red;">NÃO</a>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>


				</table>

				<a href="{{ route('usuario.view') }}" class="waves-effect waves-light btn" style="margin:2% 0% 0% 25%;background: #2979ff; width: 50%;">Cadastrar</a>
			</section>

		</div>

	</div>
</div>
</div>





@include('footer.footerAdmin')