<style type="text/css">
	.titulo{
		font-family: url(https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap);
		font-weight: bold;
	}

	.texto{
		font-family: url(https://fonts.googleapis.com/css2?family=Montserrat:wght@700&display=swap);
	}

</style>

<footer class="page-footer" style="background-color: #000000;">
	<div class="container" style="width: 100%;">
		<div class="row">


			<div class="col l6 s12">

				<div class="mapouter">
					<div class="gmap_canvas"><iframe width="535" height="515" id="gmap_canvas" src="https://maps.google.com/maps?q=Secretaria%20Municipal%2C%20rua%20Capit%C3%A3o%20Valdemar%20de%20Lima%2C%20202%20-%20Centro%20&t=&z=19&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" style="border:0;width: 100%px;height: 310px;">
						
					</iframe>
				</div>
			</div>

		</div>

		<div class="col l4 offset-l2 s12" style="margin-left: 0px;">
			<h5 class="white-text titulo">Contatos</h5>
			<p class="grey-text text-lighten-4 texto">Telefone: (85) 3521-5652</p>
			<p class="grey-text text-lighten-4 texto">Email: seduc@maracanau.ce.gov.br</p>
			<p class="grey-text text-lighten-4 texto">Endereço: Rua Capitão Valdemar de Lima, 202 -Centro- Maracanau/Ceará CEP: 61900-025 </p>
		</div>
	</div>

	<div class="footer-copyright" style="background-color:#000000;">

		<div class="container texto">
			Copyright © 2020 SEDUC. Todos os direitos reservados
		</div>
	</div>
</footer>


</body>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="{{URL::asset('materialize/js/materialize.min.js')}}"></script>

<script type="text/javascript">
	$(document).ready(function(){
		$('.sidenav').sidenav();
	});
</script>


<script>

	document.addEventListener('DOMContentLoaded', function() {
		var elems = document.querySelectorAll('.parallax');
		var instances = M.Parallax.init(elems);
	});

</script>

<script type="text/javascript">

	$(document).ready(function(){



		$(window).scroll(function(){

			if($(window).scrollTop()>390) {
				$('#nav-header').addClass('blue accent-3');
			}else{
				$('#nav-header').removeClass('blue accent-3');
			}
		});
	});



</script>




</html> 