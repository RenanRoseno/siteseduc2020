<!DOCTYPE html>
<html>
<head>
	<title></title>

	<link rel="stylesheet" type="text/css" href="{{URL::asset('materialize/css/materialize.min.css')}}">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="{{URL::asset('style/style.css')}}"> 
	

	<style type="text/css">
		
		


	.smenu a:before{
		content: "";
		position: absolute;
		width: 6px;
		height: 100%;
		background: #10ac84;
		left: 0px;
		top: 0px;
		transition: 0.3s;
		opacity: 0;
	}
	.smenu a:hover:before{
		opacity: 1;
	}
	/* importantes */

	.smenu{
		background: #2979ff;
		overflow: hidden;
		transition: max-heigth 0.3s;
		max-height: 0;
	}
	.smenu a{
		display: block;
		padding: 10px 26px;
		color: white;
		font-size: 14px;
		margin: 4px 0;
		position: relative;
	}

	.btn1{
		display: block;
		color: white;
		position: relative;
	}

	.item:target .smenu{
		max-height: 30em;
		transition: 0.5s;
	}

	header, main, footer {
		padding-left: 300px;
	}

	@media only screen and (max-width : 992px) {
		header, main, footer {
			padding-left: 0;
		}
	}

	#logoadmin{
		height: 110px;
		width: 40%;
		margin: 10px 0px -25px 80px;


	}





</style>
</head>
<body>

	<nav id="nav-header" class="admin">
		<div class="middle" class="nav-wrapper">
			<div class="menu">

				<a href="#" data-target="slide-out" class="sidenav-trigger"><i class="material-icons large">menu</i></a>

				<ul id="slide-out" class="sidenav sidenav-fixed">
					<li>
						<div>
							<img id="logoadmin" class="circle responsive-img" src="{{URL::asset('img/logol.png')}}">
						</div>
					</li>
					<li><div class="divider"></div></li>
					<li class="item" id="teste1">
						<a href="#teste1" class="btn1">Usuários</a>
						<div class="smenu">
							<a href="#">Adicionar</a>
							<a href="#">Visualizar</a>
							<a href="#">Editar</a>
							<a href="#">Excluir</a>
						</div>
					</li>
					<li><div class="divider"></div></li>
					<li class="item" id="teste2">
						<a href="#teste2" class="btn1">Páginas</a>
						<div class="smenu">
							<a href="#">Diretoria</a>
							<a href="#">Conselheiros</a>
							<a href="#">Resoluções</a>
							<a href="#">Plano de Ação</a>
							<a href="#">Pareceres</a>
						</div>
					</li>
					<li><div class="divider"></div></li>
					<li><a href="/logout"><i class="material-icons" style="margin-left: 65px;position: absolute;">exit_to_app</i>Logout</a></li>
					
				</ul>


				<!-- responsivo -->

				<ul id="slide-out" class="sidenav sidenav-fixed">
					<div class="background">
						<img id="logoadmin" class="circle responsive-img" src="{{URL::asset('img/logol.png')}}">
					</div>
					
					
					<li><div class="divider"></div></li>
					<div></div>
					<li class="item" id="teste3">
						<a href="#teste3" class="btn1">Usuários</a>
						<div class="smenu">
							<a href="#">Adicionar</a>
							<a href="#">Visualizar</a>
							<a href="#">Editar</a>
							<a href="#">Excluir</a>
						</div>
					</li>
					<li><div class="divider"></div></li>
					<li class="item" id="teste4">
						<a href="#teste4" class="btn1">Páginas</a>
						<div class="smenu">
							<a href="#">Diretoria</a>
							<a href="#">Conselheiros</a>
							<a href="#">Resoluções</a>
							<a href="#">Plano de Ação</a>
							<a href="#">Pareceres</a>
						</div>
					</li>
					<li><div class="divider"></div></li>
					<li><a href="/logout"><i class="material-icons" style="margin-left: 65px;position: absolute;">exit_to_app</i>Logout</a></li>
					
					
				</ul>

			</div>
		</div>
	</nav>


<img width="100" src="{{storage_path().'/posts/'.$postagem->id.'.jpg'}}">