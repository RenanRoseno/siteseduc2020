@include('hd.headerAdmin')

<div class="container">
	<div class="row">
		<div class="row">

			<div class="col s2">

			</div>
			<div class="col s10">
				<h2 class="header Titulo" style="margin-top: 19%; width: 100%;">Planos de Ações</h2>


				<section id="center" style=" margin: auto;	float: center;
				width: 120%;margin-left: 0%;">

				@if(session('sucesso'))
				<div class="alert alert success">
					{{session('sucesso')}}
				</div>
				@elseif(session('falha'))
				<div class="alert alert danger">
					{{session('falha')}}
				</div>
				@endif
				<table class="responsive-table">
					<thead>
						<tr>
							<th>Titulo</th>
							<th>Ano</th>
							<th>Download</th>
							<th>Excluir</th>
						</tr>
					</thead>
					<tbody>
						@foreach($planosDeAcoes as $planoDeAcao)
						<tr>
							<td>{{$planoDeAcao->titulo}}</td>
							<td>{{$planoDeAcao->ano}}</td>
							<td><a href="/crudPlanosDeAcoes/download_usuario/{{$planoDeAcao->id}}" target="blank" style="background-color: #2979ff;" class="waves-effect waves-light btn-floating"><i class="material-icons">file_download</i></a></td>

							<td><a  href="#modal{{$planoDeAcao->id}}" class="modal-trigger waves-effect waves-light btn-floating red"><i class="material-icons">delete</i></a>
							</td>

							<td>
								

								<div id="modal{{$planoDeAcao->id}}" class="modal" style="margin: 0% 0% 0% 30%;">
									<div class="modal-content">
										<h4>Deletar</h4>
										<p>tem certeza que você quer apagar esse usuário?</p>
									</div>
									<div class="modal-footer">
										<a  href="/crudPlanosDeAcoes/deletar/{{$planoDeAcao->id}}" class="modal-close waves-effect waves-light btn" style="background-color: green;">SIM</a>
										<a  href="#" class="modal-close waves-effect waves-light btn" style="background-color: red;">NÃO</a>
									</div>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>

				</table>
				<table>
					<a href="/crudPlanosDeAcoes/cadastro" class="waves-effect waves-light btn" style="background: #2979ff;position: absolute;margin-left: 0px; margin-top: 1%; ">Upload<i class="material-icons right">file_upload</i></a>
				</table>


				
			</section>


		</div>

	</div>
</div>
</div>




@include('footer.footerAdmin')





s