<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanosDeAcoes extends Model
{
    protected $table = 'planosDeAcoes';
    protected $guarded = [];
}
