<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resolucao extends Model
{
    protected $table = 'resolucao';
    protected $guarded = [];
}
