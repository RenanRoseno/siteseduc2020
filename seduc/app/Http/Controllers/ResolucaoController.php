<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Resolucao;
use DB;

class ResolucaoController extends Controller
{
    public function formCadastro()
    {

    	return view('crudResolucoes.inserirResolucoes');

    }

    public function cadastro(Request $request)
    {
        try{
            $resolucao = Resolucao::create($request->only('titulo','tipo'));
            if (isset($resolucao)) {
                $arquivo = $request->file('resolucao')->move(storage_path().'/resolucoes/',$resolucao->id.'.pdf'); 
                if (isset($arquivo)) {
                    return redirect()->back()->with('sucesso','Resolução/Parecer cadastrado');
                }
            }
            
            return redirect()->back()->with('falha','erro ao cadastrar resolução/parecer');       
        }catch(\Exception $ex){
            return redirect()->back()->with('falha',$ex->getMessage());       
        }

    }
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function deletar($id = null)
    {
        try{
            $deleteD = Resolucao::find($id);
            if (isset($deleteD)) {
                DB::beginTransaction();
                $deletou = $deleteD->delete();
                if (isset($deletou)){
                   $deletado = unlink(storage_path().'/resolucoes/'.$deleteD->id.'.pdf');
                   if (isset($deletado)) {
                        DB::commit();
                        return redirect()->back()->with('sucesso','Resolucões/Pareceres deletados com sucesso!');
                   }
                }
            }
            throw new Exception("Erro ao tentar excluir os dados!");
        }catch(\Exception $err){
            DB::rollBack();
            return redirect()->back()->with('falha','Falha ao deletar resolucões/pareceres');
        }
    }



public function download_usuario($id = 0)
{
    try
    {
        $resolucao = Resolucao::find($id);
        if (isset($resolucao)) {
            return \Response::download(storage_path().'/resolucoes/'.$id.'.pdf');
        }
        return redirect()->back()->with('falha','O arquivo não existe');       
    }
    catch(\Exception $erro)
    {
        return redirect()->back()->with('falha','Erro ao efetuar o download'); 
    }
}



public function download($id = 0)
{
    try
    {
        $resolucao = Resolucao::find($id);
        if (isset($resolucao)) {
            return \Response::download(storage_path().'/resolucoes/'.$id.'.pdf');
        }
        return redirect()->back()->with('falha','O arquivo não existe');       
    }
    catch(\Exception $erro)
    {
        return redirect()->back()->with('falha','Erro ao efetuar o download'); 
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public function visualizar($id = 0)
   {

    $resolucao = Resolucao::all();

    return view('crudResolucoes.visualizarResolucoes',compact('resolucao'));
   }

}
