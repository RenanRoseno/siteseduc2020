<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Conselheiros;
use App\Representantes;
use App\diretoria;
use App\Cargos;
use App\Resolucao;
use App\Pareceres;
use App\PlanosDeAcoes;
use App\Post;

class IndexController extends Controller
{

	public function login(){
		return view('auth.login');
	}

	public function apresentacao(){
		return view('paginas/apresentacao');
	}


	public function admin(){
		return view('Admin');
	}

	public function visualizarLheiros()
	{
		$representantes = Representantes::orderBy('id')->get();
		
		return view('paginas.conselheiros',compact('representantes'));
	}
	public function visualizarD($id = 0)
	{
		$diretorias = diretoria::all();

		return view('paginas.diretoria',compact('diretorias'));
	}
	public function visualizarPlanos(){
		$planosDeAcoes = PlanosDeAcoes::all();
		return view('paginas.planoDeAcao',compact('planosDeAcoes'));
	}
	public function planoDeAcao(){
		return view('paginas/planoDeAcao');
	}
	public function visualizarResolucao($id = 0)
	{
		$resolucao = Resolucao::all();
		return view('paginas.resolucoes',compact('resolucao'));
	}

	public function visualizarPareceres($id = 0)
	{
		$pareceres = Pareceres::all();
		return view('paginas.pareceres',compact('pareceres'));
	}

	public function lheiros(){
		return view('/Crudconselheiros/indexConselheiros');
	}
	public function Posts()
	{
		$posts = Post::orderBy('data','desc')->get();
		return view('paginas.postagens',compact('posts'));

	}

	public function visualizarPosts($id = 0)
	{
		$post = Post::find($id);
		return view('paginas.post',compact('post'));

	}

	public function home(){
		return view('/index');
	}
	
	


}
