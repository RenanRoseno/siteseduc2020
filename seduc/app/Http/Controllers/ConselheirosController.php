<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Conselheiros;
use App\Representantes;

class ConselheirosController extends Controller
{

//////////////////////////////////mostrar/////////////////////////////////////////////

	public function visualizarConselheiros($id = 0)
	{
		$conselheiros = Conselheiros::all();

		return view('Crudconselheiros.indexConselheiros',compact('conselheiros'));
	}

//////////////////////////////////cadastrar/////////////////////////////////////////////
	public function conselheiros(){
		$representantes = Representantes::all();

		$compact = compact('representantes');

		return view('Crudconselheiros.criarConselheiros',$compact);
	}

	public function criarConselheiros(Request $request){

		try {
			$conselheiros = Conselheiros::create($request->except('_token'));


			if(isset($conselheiros)){
				return redirect()->back()->with('sucesso','Membros do conselho cadastrados com sucesso.');
			}
			return redirect()->back()->with('falha','Falha ao enviar checagem!');
			
			
		} catch (\Exception $err) {
			
			return redirect()->back()->with('falha',$err->getMessage());
		}

	}

//////////////////////////////////deletar/////////////////////////////////////////////
	public function deletarConselheiros($id = null)
	{
		try{
			$deletarConselheiros = Conselheiros::find($id);
			if (isset($deletarConselheiros)) {
				$deletou = $deletarConselheiros->delete();
				if (isset($deletarConselheiros)){
					return redirect()->back()->with('sucesso','Membros do conselho deletados com sucesso.');
				}
			}
			return redirect()->back()->with('falha','Falha ao deletar membros do conselho.');
		}catch(\Exception $err){
			return redirect()->back()->with('falha','Falha ao deletar membros do conselho.');
		}
	}

///////////////////////////////////editar////////////////////////////////////////////

	public function seeConselheiros($id = null){

		try {
			$conselheiros = Conselheiros::find($id);
			$representantes = Representantes::all();
			return view('Crudconselheiros.editarConselheiros',compact('conselheiros','representantes'));
			
		} catch (\Exception $e) {
			return redirect()->back()->with('falha','Falha ao alterar pessoa');
			
		}
	}

	public function editarC(Request $req){

		try {

			$editarC = Conselheiros::find($req->id);
			if (isset($editarC)) {
				$alterado = $editarC->update($req->except('_token'));
				if (isset($editarC)) {
					return redirect()->to('Crudconselheiros/indexConselheiros')->with('sucesso','Membros do conselho alterados com sucesso!');

				}
			}
			return redirect()->back()->with('falha','Falha ao alterar membros do conselho.');

			
		} catch (Exception $e){
			return redirect()->back()->with('falha','falha ao alterar membros do conselho.');
			
		}

	}


}
