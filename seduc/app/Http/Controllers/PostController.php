<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Post;
use DB;
use URL;

class PostController extends Controller
{
	
	public function download($id = 0)
	{
		try
		{
			$resolucao = Post::find($id);
			if (isset($resolucao)) {
				return \Response::download(public_path().'/anexos/'.$id.'.pdf');
			}
			return redirect()->back()->with('falha','O arquivo não existe');       
		}
		catch(\Exception $erro)
		{
			return redirect()->back()->with('falha','Erro ao efetuar o download'); 
		}
	}
	public function postCadastro()
	{
		return view('post.inserirPost');
	}

	public function cadastro(Request $request)
	{

		try {
			$verifica = 1;
			$post = Post::create($request->only('titulo','conteudo','data'));
			if (isset($post)) {

				if ($request->file('anexo')) {
					$arquivo1 = $request->file('anexo')->move(public_path().'/anexos/',$post->id.'.pdf');	
				}
				if ($request->file('post')) {
					$arquivo = $request->file('post')->move(public_path().'/post/',$post->id.'.png');
					
				}else
				{
					return redirect()->back()->with('sucesso', ' Postagem publicada sem imagem'); 
				}

				if (isset($arquivo) && isset($arquivo1)) {
					return redirect()->back()->with('sucesso', ' Postagem publicada com sucesso!'); 
				}

				
			}
			return redirect()->back()->with('falha', ' erro ao publicar a postagem');

		} catch (Exception $e) {
			return redirect()->back()->with('falha', 'erro ao publicar a postagem');

		}

	}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function postVisualizar()
	{
		$post = Post::all();
		return view('post.visualizarPost',compact('post'));
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public function deletarP($id = null,Request $request)
	{
		try {
			$deleteP = Post::find($id);
			if (isset($deleteP)) {
				$deletou = $deleteP->delete();

				if ($request->file('post')) {
					$deletado = unlink(public_path().'/post/'.$deleteP->id.'.png');
					$deletado1 = unlink(public_path().'/anexos/'.$deleteP->id.'.pdf');
				}else
				{
					return redirect()->back()->with('sucesso', ' Postagem sem imagem deletada');
				}

				return redirect()->back()->with('sucesso', ' Postagem deletada com sucesso!');

			}
		} catch (Exception $e) {
			return redirect()->back()->with('falha','Falha ao deletar postagem');
		}
	}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	public function postEditar($id = 0){

		try {
			$posts = Post::find($id);

			return view('post.editarPost',compact('posts'));
			
		} catch (\Exception $e) {
			return redirect()->back()->with('falha',getMessage());
			
		}
	}

	public function editarPost(Request $request)
	{

		try {

			$editarPost = Post::find($request->id);
			$alterado = $editarPost->update($request->only('titulo','conteudo','data'));
			if ($request->file('post')) {
				$arquivo = $request->file('post')->move(public_path().'/post/',$post->id.'.png');

			}else
			{
				return redirect()->back()->with('sucesso', ' Postagem sem imagem editada'); 
			}

			if (isset($arquivo)) {
				return redirect()->back()->with('sucesso','Postagem alterada com sucesso');
			}



			
			return redirect()->back()->with('falha',getMessage());


		} catch (\Exception $e) {

			return redirect()->back()->with('falha',$e->getMessage());
		}
	}



}


