<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\PlanosDeAcoes;
use DB;

class PlanoDeAcaoController extends Controller
{
    public function formCadastro()
    {

    	return view('crudPlanosDeAcoes.inserirPlanoDeAcao');

    }

    public function cadastro(Request $request)
    {
        try{
            $planosDeAcoes = PlanosDeAcoes::create($request->only('titulo','ano'));
            if (isset($planosDeAcoes)) {
                $arquivo = $request->file('planosDeAcoes')->move(storage_path().'/planosDeAcoes/',$planosDeAcoes->id.'.pdf'); 
                if (isset($arquivo)) {
                    return redirect()->back()->with('sucesso','Plano de Ação inserido com sucesso');
                }
            }
            
            return redirect()->back()->with('falha','erro ao inserir PLano de Ação');       
        }catch(\Exception $ex){
            return redirect()->back()->with('falha',$ex->getMessage());       
        }

    }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function deletar($id = null)
    {
        try{
            $deleteD = PlanosDeAcoes::find($id);
            if (isset($deleteD)) {
                DB::beginTransaction();
                $deletou = $deleteD->delete();
                if (isset($deletou)){
                   $deletado = unlink(storage_path().'/planosDeAcoes/'.$deleteD->id.'.pdf');
                   if (isset($deletado)) {
                        DB::commit();
                        return redirect()->back()->with('sucesso',' Plano de Ação deletado com sucesso!');
                   }
                }
            }
            throw new Exception("Erro ao tentar excluir os dados!");
        }catch(\Exception $err){
            DB::rollBack();
            return redirect()->back()->with('falha','Falha ao deletar Plano de Ação');
        }
    }



public function download_usuario($id = 0)
{
    try
    {
        $planosDeAcoes = PlanosDeAcoes::find($id);
        if (isset($planosDeAcoes)) {
            return \Response::download(storage_path().'/planosDeAcoes/'.$id.'.pdf');
        }
        return redirect()->back()->with('falha','O arquivo não existe');       
    }
    catch(\Exception $erro)
    {
        return redirect()->back()->with('falha','Erro ao efetuar o download'); 
    }
}



public function download($id = 0)
{
    try
    {
        $planosDeAcoes = PlanosDeAcoes::find($id);
        if (isset($planosDeAcoes)) {
            return \Response::download(storage_path().'/planosDeAcoes/'.$id.'.pdf');
        }
        return redirect()->back()->with('falha','O arquivo não existe');       
    }
    catch(\Exception $erro)
    {
        return redirect()->back()->with('falha','Erro ao efetuar o download'); 
    }
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

  public function visualizar($id = 0)
   {

    $planosDeAcoes = PlanosDeAcoes::all();

    return view('crudPlanosDeAcoes.vizualizarPlanoDeAcao',compact('planosDeAcoes'));
   }

}
