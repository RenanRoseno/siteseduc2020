<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\diretoria;
use App\Cargos;

class DiretoriaController extends Controller
{
	
/////////////////////////////////inserir/////////////////////////////////////////////
	public function criarD(){
		$cargos = Cargos::all();

		$compact = compact('cargos');

		return view('crudDiretoria.Criar',$compact);
	}

	public function diretoria(Request $request){

		try {
			$diretoria = diretoria::create($request->except('_token'));
			if(isset($diretoria)){
				return redirect()->back()->with('sucesso','Membro da diretoria cadastrado com sucesso.');
			}
			return redirect()->back()->with('falha','Falha ao cadastrar.');
			
			
		} catch (\Exception $err) {
			
			return redirect()->back()->with('falha',$err->getMessage());
		}

	}

//////////////////////////////////mostrar/////////////////////////////////////////////
	public function visualizarD($id = 0)
    {
    	$diretorias = diretoria::all();
    	
    	return view('crudDiretoria.visualizarD',compact('diretorias'));
    }

//////////////////////////////////editar/////////////////////////////////////////////
    public function editarD($id = 0)

	 	{
	 		try{
	 			$editD = diretoria::find($id);
	 			$cargos = Cargos::all();
	 			return view('crudDiretoria.editarD',compact('editD','cargos'));
	 		}catch(\Exception $err){
	 			return redirect()->back()->with('falha','Falha ao buscar usuário');
	 		}
	 	}

	 public function editarDire(Request $request)
	 	{
	 		try{
	 			$editarDire = diretoria::find($request->id);
	 			if (isset($editarDire)) {
	 				$alterado = $editarDire->update($request->except('_token'));
	 				if (isset($editarDire)){
	 					return redirect()->to('/crudDiretoria/visualizarD')->with('sucesso','Diretoria editada com sucesso!');
	 				}
	 			}
	 			return redirect()->back()->with('falha','Falha ao alterar usuário');
	 		}catch(\Exception $err){
	 			return redirect()->back()->with('falha','Falha ao buscar usuário');
	 		}
	 	}

//////////////////////////////////excluir/////////////////////////////////////////////
	 	public function deletarD($id = null)
 	{
 		try{
 			$deleteD = diretoria::find($id);
 			if (isset($deleteD)) {
 				$deletou = $deleteD->delete();
 				if (isset($deleteD)){
 					return redirect()->back()->with('sucesso','');
 				}
 			}
 			return redirect()->back()->with('falha','Falha ao deletar usuário');
 		}catch(\Exception $err){
 			return redirect()->back()->with('falha','Falha ao deletar usuário');
 		}
 	}
}
