<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Pareceres;
use DB;

class PareceresController extends Controller
{
	public function visualizarPareceres($id = 0)
	{
		$pareceres = Pareceres::all();

		return view('crudPareceres.visualizarPareceres',compact('pareceres'));
	}

/////////////////////////////////////////////////////////////////////////////////////////////
	public function pareceresAdmin()
	{
		return view('crudPareceres.inserirPareceres');
	}

	public function cadastrarParecer(Request $request)
	{
		try{
			$pareceres = Pareceres::create($request->only('titulo'));
			if (isset($pareceres)) {
				$arquivo = $request->file('pareceres')->move(storage_path().'/pareceres/',$pareceres->id.'.pdf'
			);
				if (isset($arquivo)) {
					return redirect()->back()->with('sucesso','Parecer cadastrado');
				}

			}

			return redirect()->back()->with('falha','erro ao cadastrar parecer');
		}catch(\Exception $ex){
			return redirect()->back()->with('falha',$ex->getMessage());       
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////
	public function excluirParecer($id = null)
	{
		try{
			$excluirP = Pareceres::find($id);
			if (isset($excluirP)) {
				DB::beginTransaction();
				$excluiu = $excluirP->delete();
				if (isset($excluiu)) {
					$deletado = unlink(storage_path().'/pareceres/'.$excluirP->id.'.pdf');
					if (isset($deletado)) {
						DB::commit();
						return redirect()->back()->with('sucesso','Parecer deletados com sucesso!');
					}
				}
			}
			throw new Exception("Erro ao tentar excluir os dados!");
		}catch(\Exception $err){
			DB::rollBack();
			return redirect()->back()->with('falha','Falha ao deletar parecer');
		}
	}

////////////////////////////////////////////////////////////////////////////////////////////////
	public function downloadAdmin($id = 0)
	{
		try{
			$pareceres = Pareceres::find($id);
			if (isset($pareceres)) {
				return \Response::download(storage_path().'/pareceres/'.$id.'.pdf');
			}
			return redirect()->back()->with('falha','O arquivo não existe');
		
		}catch(\Exception $erro){
			return redirect()->back()->with('falha','Erro ao efetuar o download'); 
		}
	}

	public function downloadUsuario($id = 0)
	{
		try{
			$pareceres = Pareceres::find($id);
			if (isset($pareceres)) {
				return \Response::download(storage_path().'/pareceres/'.$id.'.pdf');
			}
			return redirect()->back()->with('falha','O arquivo não existe');
		
		}catch(\Exception $erro){
			return redirect()->back()->with('falha','Erro ao efetuar o download'); 
		}
	}
}
