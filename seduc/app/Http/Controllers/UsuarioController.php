<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests;
use App\Tipos;
use Auth;

class UsuarioController extends Controller
{



//-----------------------inserir------------------------------------------------------------------

	public function Inserir(){

		$tipos = Tipos::all();

		$compact = compact('tipos');


		return view('admin.crud.inserir',$compact);
	}


	public function inserirUser(Request $request){

		try {
			$request->merge(['password' => bcrypt($request->password)]);
			$usuarios = User::create($request->except('_token'));
			if(isset($usuarios)){
				return redirect()->back()->with('sucesso','Usuário inserido com sucesso!');
			}
			return redirect()->back()->with('falha','ERRO, usuário já existe');
		} catch (\Exception $e) {
			
			return redirect()->back()->with('falha','ERRO, usuário não cadastrado');
		}

	}
//----------------------------------------------------------------------------------------------


//-----------------------------visualizar-------------------------------------------------------

	public function visualizar(){
		$usuarios = User::all();
		return view('admin.crud.visualizar',compact('usuarios'));

	}







//----------------------------------------------------------------------------------------------


//-----------------------------editar-------------------------------------------------------

	public function editarV($id = 0){

		try{
			$editar = User::find($id);
			$tipos = Tipos::all();
			return view('admin.crud.editarV',compact('editar','tipos'));
		}catch(\Exception $err){
			return redirect()->back()->with('falha','Falha ao buscar usuário');
		}

	}

	public function editarUser(Request $request)
	{
		try{
			$request->merge(['password' => bcrypt($request->password)]);
			$editarUser = User::find($request->id);
			if (isset($editarUser)) {
				$alterado = $editarUser->update($request->except('_token'));
				if (isset($editarUser)){
					return redirect()->to('admin/visualizar')->with('sucesso','Usuário editado com sucesso!');
				}
			}
			return redirect()->back()->with('falha','Falha ao alterar usuário');
		}catch(\Exception $err){
			return redirect()->back()->with('falha','Falha ao buscar usuário');
		}
	}






//----------------------------------------------------------------------------------------------

//------------------------------------delete-----------------------------------------------------

	public function deletar($id = null)
	{
		try{
			$delete = User::find($id);
			$usuarioLogado = Auth::user();

			if (isset($delete)) {
				if ($delete->id == $usuarioLogado->id) {
					return redirect()->back()->with('falha','Falha, o usuário esta logado');				
				}
				$deletou = $delete->delete();
				if (isset($delete)){
					return redirect()->back()->with('sucesso','');
				}
			}
			return redirect()->back()->with('falha','Falha ao deletar usuário');
		}catch(\Exception $err){
			return redirect()->back()->with('falha','Falha ao deletar usuário');
		}
	}


//-----------------------------------------------------------------------------------------------



}
