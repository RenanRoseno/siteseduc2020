<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/admin', function () {
	return view('auth.login')->name('login');
});



Route::get('/crudResolucoes/download/{id?}','ResolucaoController@download');
Route::get('/crudPareceres/downloadUsuario/{id?}','PareceresController@downloadUsuario');
Route::get('/crudPlanosDeAcoes/download/{id?}','PlanoDeAcaoController@download');



Route::get('/','IndexController@home')->name('home');


Route::get('/apresentacao','IndexController@apresentacao')->name('apresentacao');
Route::get('/sistemas','SistemaController@index')->name('sistemas');

Route::get('/conselheiros','IndexController@visualizarLheiros')->name('conselheiros');
Route::get('/diretoria','IndexController@visualizarD')->name('diretoria');
Route::get('/resolucoes','IndexController@visualizarResolucao')->name('resolucoes');
Route::get('/pareceres','IndexController@visualizarPareceres')->name('pareceres');
Route::get('/planoDeAcao','IndexController@visualizarPlanos')->name('planoDeAcao');
Route::get('/postagens','IndexController@Posts')->name('postagens');
Route::get('/posts/{id?}','IndexController@visualizarPosts')->name('postagem.id');

Route::get('/crudResolucoes/download/{id?}','ResolucaoController@download');
Route::get('/anexo/download/{id}', 'PostController@download')->name('anexo.download');

Route::auth();

Route::group(['middleware' => 'auth'], function(){

	Route::get('/admin','HomeController@index')->name('admin');


//--------------------------inserir-----------------------------------

	Route::get('/admin/inserir', 'UsuarioController@inserir')->name('usuario.view');
	Route::post('/admin/inserir','UsuarioController@inserirUser')->name('usuario.salvar');

	Route::get('crudDiretoria/criarD','DiretoriaController@criarD');
	Route::post('/crudDiretoria/criarD','DiretoriaController@diretoria');

	Route::get('Crudconselheiros/criarConselheiros','ConselheirosController@conselheiros');
	Route::post('Crudconselheiros/criarConselheiros','ConselheirosController@criarConselheiros');


	Route::get('/crudResolucoes/cadastro','ResolucaoController@formCadastro');
	Route::post('/crudResolucoes/cadastro','ResolucaoController@cadastro');

	Route::get('/crudPareceres/inserirPareceres','PareceresController@pareceresAdmin');
	Route::post('/crudPareceres/cadastrarParecer','PareceresController@cadastrarParecer');

	Route::get('/crudPlanosDeAcoes/cadastro','PlanoDeAcaoController@formCadastro');
	Route::post('/crudPlanosDeAcoes/cadastro','PlanoDeAcaoController@cadastro');


	Route::get('/admin/inserirPost','PostController@postCadastro')->name('noticia.view');
	Route::post('admin/inserirPost','PostController@cadastro')->name('noticia.inserir');
//--------------------------------------------------------------------


//-------------------------visualizar---------------------------------

	Route::get('/admin/visualizar','UsuarioController@visualizar')->name('usuarios.visualizar');

	Route::get('/crudDiretoria/visualizarD','DiretoriaController@visualizarD');


	Route::get('/Crudconselheiros/indexConselheiros','ConselheirosController@visualizarConselheiros');


	Route::get('/crudResolucoes/visualizarResolucoes','ResolucaoController@visualizar');


	Route::get('/crudPareceres/visualizarPareceres','PareceresController@visualizarPareceres');



	Route::get('/crudPlanosDeAcoes/visualizarPlanoDeAcao','PlanoDeAcaoController@visualizar');

	Route::get('/admin/visualizarPost','PostController@postVisualizar')->name('noticias.visualizar');


	Route::get('/admin/logout', 'Auth\AuthController@getLogout')->name('logout');




//--------------------------------------------------------------------


//------------------------------download-----------------------------

	Route::get('/crudResolucoes/download_usuario/{id?}','ResolucaoController@download_usuario');

	Route::get('/crudPareceres/downloadAdmin/{id?}','PareceresController@downloadAdmin');
	

	Route::get('/crudPlanosDeAcoes/download_usuario/{id?}','PlanoDeAcaoController@download_usuario');




//------------------------------------------------------------------


//-------------------------editar---------------------------------

	Route::get('/admin/editarV/{id?}','UsuarioController@editarV')->name('usuario.editar');
	Route::post('/admin/editarV','UsuarioController@editarUser')->name('usuario.atualizar');


	Route::get('/editarD/{id?}','DiretoriaController@editarD');
	Route::post('/editarD/editD','DiretoriaController@editarDire');

	Route::get('/editarConselheiros/{id?}','ConselheirosController@seeConselheiros');
	Route::post('/editarConselheiros','ConselheirosController@editarC');

	Route::get('/editarPost/{id?}','PostController@postEditar')->name('noticia.editar');
	Route::post('/editarPost','PostController@editarPost')->name('noticia.atualizar');

//--------------------------------------------------------------------

//-------------------------excluir---------------------------------

	Route::get('/crudDiretoria/deletarD{id?}','DiretoriaController@deletarD');

	Route::get('/admin/deletar/{id?}','UsuarioController@deletar')->name('usuario.excluir');

	Route::get('/Crudconselheiros/deletarConselheiros/{id?}','ConselheirosController@deletarConselheiros');

	Route::get('/crudResolucoes/deletar/{id?}','ResolucaoController@deletar');

	Route::get('/crudPareceres/excluir/{id?}','PareceresController@excluirParecer');

	Route::get('crudPlanosDeAcoes/deletar/{id?}','PlanoDeAcaoController@deletar');

	Route::get('/admin/deletarP/{id?}','PostController@deletarP')->name('noticia.excluir');


//----------------------------------------------------------------





});
