<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pareceres extends Model
{
   protected $table = 'pareceres';
   protected $guarded = [];
}
