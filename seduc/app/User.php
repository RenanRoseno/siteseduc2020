<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Tipos;


class User extends Authenticatable
{

    protected $table ='usuarios';

    public function dados(){

        return $this->hasOne(Tipos::class,'id','id_tipo');
    } 

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','id_tipo'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
