<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Representantes;

class Conselheiros extends Model
{
    protected $table = 'conselheiros';
    protected $guarded = [];

    public function representantes()
    {
    	return $this->hasOne(Representantes::class,'id','id_representante');
    }
  
}
