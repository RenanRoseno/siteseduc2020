<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Cargos;

class diretoria extends Model
{
    protected $table ='diretorias';
    protected $guarded = [];

    public function dados(){
    	return $this->hasOne(Cargos::class,'id','id_cargo');
    } 
}

