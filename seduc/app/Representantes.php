<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Conselheiros;

class Representantes extends Model
{
    protected $table = 'representantes';
    protected $guarded = [];

    public function conselheiros()
    {
    	return $this->hasMany(Conselheiros::class,'id_representante','id');
    }


}
