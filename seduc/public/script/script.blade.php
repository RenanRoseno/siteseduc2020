<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript" src="{{URL::asset('materialize/js/materialize.min.js')}}"></script>

<script>

	document.addEventListener('DOMContentLoaded', function() {
		var elems = document.querySelectorAll('.parallax');
		var instances = M.Parallax.init(elems);
	});

</script>

<script type="text/javascript">

	$(document).ready(function(){

		$(".dropdown-trigger").dropdown();


		$(window).scroll(function(){

			if($(window).scrollTop()>420) {
				$('#nav-header').addClass('blue accent-3');
			}else{
				$('#nav-header').removeClass('blue accent-3');
			}
		});
	});
</script>



